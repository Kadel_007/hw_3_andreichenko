# I. Сформировать строку, в которой содержится информация об определенном слове в строке.
# Например "The [номер] symbol in [тут слово] is [значение символа по номеру в слове]".
# Слово и номер получите с помощью input() или воспользуйтесь константой.
# Например (если слово - "Python" а символ 3) - "The 3 symbol in "Python" is t".

# II. Ввести из консоли строку. Определить количество слов в этой строке,
# которые заканчиваются на букву "o" (учтите, что буквы бывают заглавными).

# Есть list с данными lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишите механизм, который формирует новый list (например lst2), который содержит только переменные-строки, которые есть в lst1.

# I
def symbolInText():

    print("Input whole number (>=1): ")
    number = input()

    if number.isdigit():
        number = int(number)
        if number == 0:
            print("Can't find the symbol in zero index.")
            return
    else:
        print("Wrong input format.")
        return

    print("Input word: ")
    str = input()

    str_len = len(str)
    found = ""
    for i in range(str_len):
        if i + 1 == number:
            found = str[i]
            break

    if found != "":
        print("The " + "\033[1m" + f"{number} " + "\033[0m" + "symbol in " + "\033[1m" + f"{str}" + "\033[0m" + f" is " + "\033[1m"+ f"{found}")

    else:
        print("Haven't found the symbol with number " + "\033[1m" + f"{number} " + "\033[0m" + "in " + "\033[1m" + f"{str}")

# II
def endStringWithO():

    print("Input string: ")
    str = input()
    lst = str.split(" ")
    count = 0

    for i in range(len(lst)):
        if lst[i].endswith("o") or lst[i].endswith("O"):
            count += 1

    print("Count of words that end with o or O: ", count)

# III 
def newList():

    lst = ["Zero", 1, 2, None, "Four", "Zer0 + Five", True, "?", 8, "Yeah, that's true", False]
    newlst = []

    print("Your List:")
    print(lst)
    print("\n")

    for i in range(len(lst)):
        if isinstance(lst[i], str):
            newlst.append(lst[i])


    print("NEW List:")
    print(newlst)



def main():
    # I (comment / uncomment)
    #symbolInText()
    # II (comment / uncomment)
    #endStringWithO()
    # III (comment / uncomment)
    newList()


main()